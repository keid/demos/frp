{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE OverloadedLists #-}

module Stage.ReactiveBanana.Network.Window
  ( allocate
  ) where

import RIO.Local

import DearImGui qualified as ImGui
import Engine.ReactiveBanana qualified as Network
import Engine.ReactiveBanana.Window qualified as Window
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Window.MouseButton qualified as MouseButton
import Engine.Worker qualified as Worker
import Geomancy.Transform ((!.))
import Geomancy.Transform qualified as Transform
import Geometry.Hit qualified as Hit
import Geometry.Intersect qualified as Intersect
import Geometry.Plane qualified as Plane
import Geometry.Ray qualified as Ray
import Reactive.Banana ((<@>))
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks qualified as RBF
import Render.DescSets.Set0 (Scene(..))
import RIO.State (get)
import Vulkan.Core10 qualified as Vk

import Stage.ReactiveBanana.Types (RunState(..))

allocate :: ResourceT (StageRIO RunState) RBF.EventNetwork
allocate = do
  cursorPos <- Window.allocateCursorPos
  fromMouseButton <- Window.allocateMouseButton
  rs <- get

  Network.allocateActuated \(UnliftIO unlift) _startEv -> mdo
    let
      reactimateDebugShow :: Show a => RB.Event a -> RBF.MomentIO ()
      reactimateDebugShow = RBF.reactimate . fmap (unlift . logDebug . displayShow)

    imguiCaptureMouse <- RBF.fromPoll ImGui.wantCaptureMouse

    cursorPosE <- cursorPos
    cursorPosB <- RB.stepper (0, 0) cursorPosE

    mouseButtonE <- RB.whenE (fmap not imguiCaptureMouse) <$> fromMouseButton

    -- XXX: Set up event fusion, driven by mouseButtonE
    mouseButtons' <- sequenceA @MouseButton.Collection $ pure RBF.newEvent
    let mouseClicked = fmap fst mouseButtons'
    let
      dispatchButtons pos (mods, state, mb) =
        MouseButton.whenPressed state $
          -- XXX: Use one event handler to drive multiple derived events
          snd (MouseButton.atGlfw mouseButtons' mb) (mods, pos)
    RBF.reactimate $
      dispatchButtons <$> cursorPosB <@> mouseButtonE

    for_ (zip (toList MouseButton.collectionGlfw) (toList mouseClicked)) \(btn, clickE) ->
      reactimateDebugShow $ fmap (btn,) clickE

    screenSize <- liftIO (unlift Engine.askScreenVar) >>=
      RBF.fromPoll . Worker.getOutputData
    let
      halfSize =
        screenSize <&>
          \Vk.Extent2D{width, height} ->
            ( fromIntegral width / 2
            , fromIntegral height / 2
            )

    let cursorPosCenteredE = centerCursor <$> halfSize <@> cursorPosE
    Network.pushWorkerInput (rsCursorPos rs) cursorPosCenteredE -- XXX: debug-ish output

    let
      cursorPosNormalizedE =
        (\(hx, hy) (cx, cy) -> (cx / hx, cy / hy))
          <$> halfSize
          <@> cursorPosCenteredE

    scenePlaneCursor rs cursorPosNormalizedE

centerCursor :: Fractional a => (a, a) -> (a, a) -> (a, a)
centerCursor (halfWidth, halfHeight) (wx, wy) =
  ( wx - halfWidth
  , wy - halfHeight
  )

scenePlaneCursor
  :: RunState
  -> RB.Event (Double, Double)
  -> RBF.MomentIO ()
scenePlaneCursor RunState{..} cursorPosNormalizedE = do
  scene <- RBF.fromPoll $ Worker.getOutputData rsSceneP

  let
    inverseToWorld = scene <&> \s ->
      sceneInvProjection s <> sceneInvView s

    cRay = cursorRay <$> scene <*> inverseToWorld <@> cursorPosNormalizedE

    plane = Plane.xzUp 0 -- (y + 0.5)
    planePosE = cRay <&> \r ->
      fmap Hit.position $
        Intersect.ray'plane r plane

  Network.pushWorkerInput rsPlanePos planePosE -- XXX: debug-ish output

  Network.pushWorkerOutput rsPlaneCursor $ flip fmap planePosE \case
    Nothing ->
      []
    Just pos ->
      [ Transform.scale 0.5 <>
        Transform.translateV pos
      , withVec3 pos \x y z ->
          Transform.translate
            (fromInteger $ round x)
            (fromInteger $ round y)
            (fromInteger $ round z)
      ]

cursorRay :: Scene -> Transform -> (Double, Double) -> Ray.Ray
cursorRay s inverse (cx, cy) = Ray.fromSegment origin target
  where
    origin = inverse !. vec3 x y near -- Camera.PROJECTION_NEAR
    target = inverse !. vec3 x y far -- Camera.PROJECTION_FAR

    x = double2Float cx
    y = double2Float cy

    WithVec4 near far _ _ = sceneTweaks s
