module Stage.ReactiveBanana.World.Mesh where

import RIO.Local

import Engine.Vulkan.Types (MonadVulkan, Queues)
import Geomancy.Transform qualified as Transform
import Geometry.Cube qualified as Cube
import Render.Unlit.Colored.Model qualified as Unlit
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Resource qualified as ResourceT
import Vulkan.Core10 qualified as Vk

type Input = Int

type Output = Storable.Vector Transform
type Observer = Buffer.ObserverCoherent Transform

type Model = Unlit.Model 'Buffer.Staged

mkOutput :: Vec3 -> Input -> Output
mkOutput origin n = instances
  where
    instances = Storable.fromList do
      ix <- [1 .. fromIntegral n]
      withVec3 origin \x y z ->
        pure $ Transform.translate (x + ix - 1) y z

mkModel
  :: ( MonadVulkan env m
     , ResourceT.MonadResource m
     )
  => Queues Vk.CommandPool
  -> m Model
mkModel queues = do
  model <- Model.createStagedL (Just "bbWireColored") queues Cube.bbWireColored Nothing
  Model.registerIndexed_ model
  pure model
